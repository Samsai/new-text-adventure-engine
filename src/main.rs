
#[macro_use] extern crate hmap;
#[macro_use] extern crate str_macro;
extern crate anymap;

mod engine;
mod game;

use engine::play_round;
use engine::TerminalIO;
use game::init_game;

fn main() {
    let mut game = init_game(Box::new(TerminalIO));

    'parser: loop {
        play_round(&mut game);
    }
}
