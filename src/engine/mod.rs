
use std::io;
use std::io::Write;

use std::collections::HashMap;

use anymap::AnyMap;

#[derive(Debug)]
enum Command {
    North,
    South,
    East,
    West,

    Get(String),
    Drop(String),

    LookAround,
    Look(String),

    Use(String),
    UseOn(String, String)
}

fn parse_command(input: String) -> Option<Command> {
    use Command::*;

    // TODO: Expand as necessary
    let delimiter_words = vec!["at", "to", "with", "on"];

    let mut parts_iter = input.split_whitespace();

    let verb = parts_iter.next()?;
    let mut rest: String = parts_iter
        .map(|w| if delimiter_words.contains(&w) { "|" } else { w })
        .fold(String::new(), |mut s, n| {
            s.push_str(&format!("{} ", n));
            s
        });

    let objects: Vec<&str> = rest
        .split("|")
        .filter(|o| o.len() > 0)
        .map(|s| s.trim())
        .collect();

    // println!("{:?}", objects);

    return match verb {
        "north" => Some(North),
        "south" => Some(South),
        "east" => Some(East),
        "west" => Some(West),
        "get" => Some(Get(String::from(objects[0]))),
        "take" => Some(Get(String::from(objects[0]))),
        "drop" => Some(Drop(String::from(objects[0]))),
        "look" =>
            if objects.is_empty() {
                Some(LookAround)
            } else {
                Some(Look(String::from(objects[0])))
            },
        "use" =>
            if objects.is_empty() {
                None
            } else if objects.len() == 1 {
                Some(Use(String::from(objects[0])))
            } else {
                Some(UseOn(String::from(objects[0]), String::from(objects[1])))
            },
        _ => None
    }
}

#[derive(Hash, PartialEq, Eq)]
pub enum Direction {
    North,
    South,
    East,
    West,
}

pub struct Item {
    pub name: String,
    pub aliases: Vec<String>,
    pub description: String,
    pub can_be_moved: bool,
    pub use_function: &'static dyn Fn(&mut Item, &mut dyn IOAbstraction, &mut Player, &mut Scene),
    pub use_with_function: &'static dyn Fn(&mut Item, &mut dyn IOAbstraction, &mut Player, &mut Scene, &str),
}

pub struct Scene {
    pub description: String,
    pub exits: HashMap<Direction, (String, &'static str)>,
    pub items: Vec<String>,
    pub update: &'static dyn Fn(&mut dyn IOAbstraction, &mut Player, &mut Scene),
}

pub struct Player {
    pub inventory: Vec<String>,
    pub current_scene: &'static str,
    pub variables: AnyMap,
}

enum Tag {
    Wait(f32)
}

fn parse_tag(string: &str) -> Option<Tag> {
    let parts: Vec<&str> = string.split_whitespace().collect();

    match parts[0] {
        "wait" => Some(Tag::Wait(parts[1].parse::<f32>().unwrap())),
        _ => None
    }
}

pub trait IOAbstraction {
    fn print_line(&mut self, line: &str);
    fn print(&mut self, string: &str);
    fn read_line(&mut self) -> String;
}

pub struct TerminalIO;

impl IOAbstraction for TerminalIO {
    fn print_line(&mut self, line: &str) {
       self.print(&format!("{}\n", line));
    }

    fn print(&mut self, string: &str) {
        let mut characters = string.chars();

        while let Some(c) = characters.next() {
            // Handle control tags if we find opening brace
            if c == '{' {
                let tag_string: String = characters.clone().take_while(|c| *c != '}').collect();

                if let Some(tag) = parse_tag(&tag_string) {
                    match tag {
                        Tag::Wait(amount) => std::thread::sleep(std::time::Duration::from_secs_f32(amount)),
                    }
                }

                // Since the tag_string is built out of a clone of the iterator
                // we need to skip the iterator forward to the closing brace
                while characters.next().unwrap() != '}' {}
            } else {
                // Otherwise we just print the character and flush
                print!("{}", c);
                io::stdout().flush();
            }
        }
    }

    fn read_line(&mut self) -> String {
        let mut input = String::new();
        io::stdout().flush();
        io::stdin().read_line(&mut input);

        return input;
    }
}

pub struct Game {
    pub io: Box<dyn IOAbstraction>,
    pub items: HashMap<String, Item>,
    pub scenes: HashMap<&'static str, Scene>,
    pub player: Player,
}

impl Game {
    pub fn new(io: Box<dyn IOAbstraction>) -> Game {
        Game {
            io,
            items: HashMap::new(),
            scenes: HashMap::new(),
            player: Player {
                inventory: Vec::new(),
                current_scene: "",
                variables: AnyMap::new(),
            }
        }
    }
}

fn resolve_aliases(player: &Player, scene: &Scene, items: &HashMap<String, Item>, command: Command) -> Command {
    use Command::*;

    return match command {
        Get(item) => {
            for i in &scene.items {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item) {
                    return Get(i.clone());
                }
            }

            return Get(item);
        },
        Drop(item) => {
            for i in &player.inventory {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item) {
                    return Drop(i.clone());
                }
            }

            return Drop(item);
        },
        Look(item) => {
            for i in &player.inventory {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item) {
                    return Look(i.clone());
                }
            }

            for i in &scene.items {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item) {
                    return Look(i.clone());
                }
            }

            return Look(item);
        },
        Use(item) => {
            for i in &player.inventory {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item) {
                    return Use(i.clone());
                }
            }

            for i in &scene.items {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item) {
                    return Use(i.clone());
                }
            }

            return Use(item);
        },
        UseOn(item1, item2) => {
            let mut alias1 = item1.clone();

            for i in &player.inventory {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item1) {
                    alias1 = i.clone();
                }
            }

            for i in &scene.items {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item1) {
                    alias1 = i.clone();
                }
            }

            let mut alias2 = item2.clone();

            for i in &player.inventory {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item2) {
                    alias2 = i.clone();
                }
            }

            for i in &scene.items {
                let game_item = items.get(i).unwrap();

                if game_item.aliases.contains(&item2) {
                    alias2 = i.clone();
                }
            }

            return UseOn(alias1, alias2);
        }
        _ => command
    }
}

pub fn play_round(game: &mut Game) {
    let mut io = &mut game.io;
    let mut scene = game.scenes.get_mut(&game.player.current_scene).unwrap();

    io.print_line(&format!("\n{}", scene.description));

    io.print_line("A couple of things catch your eye:");
    for i in &scene.items {
        io.print_line(&format!("- {}", game.items.get(i).unwrap().name));
    }

    (scene.update)(io.as_mut(), &mut game.player, &mut scene);

    io.print("> ");

    let mut input = io.read_line();

    input = input.to_lowercase();

    if let Some(command) = parse_command(input) {
        use Command::*;

        let command = resolve_aliases(&game.player, scene, &game.items, command);

        match command {
            North => {
                if let Some(place) = scene.exits.get(&Direction::North) {
                    game.player.current_scene = place.1.clone();
                } else {
                    io.print_line("Can't go that way!");
                }
            },
            South => {
                if let Some(place) = scene.exits.get(&Direction::South) {
                    game.player.current_scene = place.1.clone();
                } else {
                    io.print_line("Can't go that way!");
                }
            },
            East => {
                if let Some(place) = scene.exits.get(&Direction::East) {
                    game.player.current_scene = place.1.clone();
                } else {
                    io.print_line("Can't go that way!");
                }
            },
            West => {
                if let Some(place) = scene.exits.get(&Direction::West) {
                    game.player.current_scene = place.1.clone();
                } else {
                    io.print_line("Can't go that way!");
                }
            },
            Get(s) => {
                if scene.items.contains(&s) && game.items.get(&s).unwrap().can_be_moved {
                    scene.items.retain(|i| *i != s);

                    game.player.inventory.push(s);

                    io.print_line("Picked up.");
                } else {
                    io.print_line("You can't take that!");
                }
            },
            Drop(s) => {
                if game.player.inventory.contains(&s) {
                    game.player.inventory.retain(|i| *i != s);

                    scene.items.push(s);

                    io.print_line("Dropped.");
                }
            },
            LookAround => {
                // TODO: Implement when partial descriptions are done
            },
            Look(s) => {
                if scene.items.contains(&s) || game.player.inventory.contains(&s) {
                    io.print_line(&format!("{}", game.items.get(&s).unwrap().description));
                }
            },
            Use(s) => {
                if scene.items.contains(&s) || game.player.inventory.contains(&s) {
                    let mut item = game.items.get_mut(&s).unwrap();

                    (item.use_function)(&mut item, io.as_mut(), &mut game.player, &mut scene);
                }
            },
            UseOn(s1, s2) => {
                if scene.items.contains(&s1) || game.player.inventory.contains(&s1) {
                    if scene.items.contains(&s2) || game.player.inventory.contains(&s2) {
                        let mut item = game.items.get_mut(&s1).unwrap();

                        (item.use_with_function)(&mut item, io.as_mut(), &mut game.player, &mut scene, &s2);
                    }
                }
            },
        }
    } else {
        io.print_line("Didn't quite catch that.");
    }

}
