
use crate::engine::*;

struct Score(u32);

pub fn init_game(io: Box<dyn IOAbstraction>) -> Game {
    use Direction::*;

    let mut game = Game::new(io);

    game.player.current_scene = "basketball court";

    game.player.variables.insert(Score(0));

    game.scenes.insert("basketball court", Scene {
        description: str!("You are in a dark basketball court."),
        exits: hmap!(
            North => (str!("a door"), "toilet")
        ),
        items: vec![str!("ball"), str!("hoop")],
        update: &|io, player, scene| {
            io.print_line("It's quiet here.");
        }
    });

    game.items.insert(str!("ball"), Item {
        name: str!("ball"),
        aliases: vec![str!("basketball"), str!("ball")],
        description: str!("It's round."),
        can_be_moved: true,
        use_function: &|s, io, player, scene| {
            io.print_line("You bounce the ball a couple of times.");
        },
        use_with_function: &|s, io, player, scene, item| {
            if item == "hoop" {
                io.print_line("You score! {wait 1.0}Fantastic!{wait 1.0}");

                let mut score = player.variables.get_mut::<Score>().unwrap();
                score.0 += 1;

                io.print_line(&format!("You've scored {} times!", score.0));

                if player.inventory.contains(&str!("ball")) {
                    player.inventory.retain(|i| *i != "ball");
                    scene.items.push(str!("ball"));
                }

                io.print_line("The ball lands underneath the hoop.");
            }
        }
    });

    game.items.insert(str!("hoop"), Item {
        name: str!("basketball hoop"),
        aliases: vec![str!("basketball hoop"), str!("hoop")],
        description: str!("Something to throw balls into."),
        can_be_moved: false,
        use_function: &|s, io, player, scene| {
            io.print_line(".{wait 0.5}.{wait 0.5}.{wait 0.5} You can't quite reach the hoop.")
        },
        use_with_function: &|s, io, player, scene, item| {
        }
    });

    game.scenes.insert("toilet", Scene {
        description: str!("You are in an even darker toilet."),
        exits: hmap!(
            South => (String::from("door leading to the court"), "basketball court")
        ),
        items: vec![],
        update: &|io, player, scene| {

        }
    });


    return game;
}
